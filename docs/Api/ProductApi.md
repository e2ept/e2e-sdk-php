# Swagger\Client\ProductApi

All URIs are relative to *https://e2e.pt/ws*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productCategoryList**](ProductApi.md#productCategoryList) | **GET** /product/category | List records
[**productId**](ProductApi.md#productId) | **GET** /product/id/{id} | Get record
[**productList**](ProductApi.md#productList) | **GET** /product | List records


# **productCategoryList**
> \Swagger\Client\Model\ProductCategoryListContainer productCategoryList()

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->productCategoryList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productCategoryList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\ProductCategoryListContainer**](../Model/ProductCategoryListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productId**
> \Swagger\Client\Model\Product productId($id)

Get record



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->productId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\Product**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productList**
> \Swagger\Client\Model\ProductListContainer productList($offset, $limit, $name, $published, $update_date, $product_category, $deleted, $ids)

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offset = 0; // int | 
$limit = 100; // int | 
$name = "name_example"; // string | 
$published = true; // bool | 
$update_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601
$product_category = "product_category_example"; // string | 
$deleted = true; // bool | 
$ids = array("ids_example"); // string[] | 

try {
    $result = $apiInstance->productList($offset, $limit, $name, $published, $update_date, $product_category, $deleted, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **name** | **string**|  | [optional]
 **published** | **bool**|  | [optional]
 **update_date** | **\DateTime**| ISO 8601 | [optional]
 **product_category** | **string**|  | [optional]
 **deleted** | **bool**|  | [optional]
 **ids** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductListContainer**](../Model/ProductListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

