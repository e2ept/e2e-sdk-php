# Swagger\Client\PriceApi

All URIs are relative to *http://localhost:8001/e2e-server-ext/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**priceList**](PriceApi.md#priceList) | **GET** /price | List records
[**priceTypeList**](PriceApi.md#priceTypeList) | **GET** /price/type | List records


# **priceList**
> \Swagger\Client\Model\PriceListContainer priceList($offset, $limit, $price_type, $currency, $unit, $product_ids)

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PriceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offset = 0; // int | 
$limit = 100; // int | 
$price_type = "price_type_example"; // string | 
$currency = "currency_example"; // string | 
$unit = "unit_example"; // string | 
$product_ids = array("product_ids_example"); // string[] | 

try {
    $result = $apiInstance->priceList($offset, $limit, $price_type, $currency, $unit, $product_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PriceApi->priceList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **price_type** | **string**|  | [optional]
 **currency** | **string**|  | [optional]
 **unit** | **string**|  | [optional]
 **product_ids** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PriceListContainer**](../Model/PriceListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **priceTypeList**
> \Swagger\Client\Model\PriceTypeListContainer priceTypeList()

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PriceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->priceTypeList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PriceApi->priceTypeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\PriceTypeListContainer**](../Model/PriceTypeListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

