# Swagger\Client\DocumentApi

All URIs are relative to *https://e2e.pt/ws*

Method | HTTP request | Description
------------- | ------------- | -------------
[**documentCreate**](DocumentApi.md#documentCreate) | **POST** /document | Create record
[**documentId**](DocumentApi.md#documentId) | **GET** /document/id/{id} | Get record
[**documentList**](DocumentApi.md#documentList) | **GET** /document | List records
[**documenttypeList**](DocumentApi.md#documenttypeList) | **GET** /document/type | List records


# **documentCreate**
> \Swagger\Client\Model\Document documentCreate($body)

Create record



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DocumentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Document(); // \Swagger\Client\Model\Document | 

try {
    $result = $apiInstance->documentCreate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentApi->documentCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Document**](../Model/Document.md)|  |

### Return type

[**\Swagger\Client\Model\Document**](../Model/Document.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **documentId**
> \Swagger\Client\Model\Document documentId($id)

Get record



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DocumentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->documentId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentApi->documentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\Document**](../Model/Document.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **documentList**
> \Swagger\Client\Model\DocumentListContainer documentList($offset, $limit, $number, $start_date, $end_date, $type1, $type2, $update_date, $account, $product, $product_category, $deleted, $name, $type, $serie, $salesman)

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DocumentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offset = 0; // int | 
$limit = 100; // int | 
$number = 56; // int | 
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601
$type1 = "type1_example"; // string | 
$type2 = "type2_example"; // string | 
$update_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601
$account = "account_example"; // string | 
$product = "product_example"; // string | 
$product_category = "product_category_example"; // string | 
$deleted = true; // bool | 
$name = "name_example"; // string | 
$type = array("type_example"); // string[] | 
$serie = array("serie_example"); // string[] | 
$salesman = array("salesman_example"); // string[] | 

try {
    $result = $apiInstance->documentList($offset, $limit, $number, $start_date, $end_date, $type1, $type2, $update_date, $account, $product, $product_category, $deleted, $name, $type, $serie, $salesman);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentApi->documentList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **number** | **int**|  | [optional]
 **start_date** | **\DateTime**| ISO 8601 | [optional]
 **end_date** | **\DateTime**| ISO 8601 | [optional]
 **type1** | **string**|  | [optional]
 **type2** | **string**|  | [optional]
 **update_date** | **\DateTime**| ISO 8601 | [optional]
 **account** | **string**|  | [optional]
 **product** | **string**|  | [optional]
 **product_category** | **string**|  | [optional]
 **deleted** | **bool**|  | [optional]
 **name** | **string**|  | [optional]
 **type** | [**string[]**](../Model/string.md)|  | [optional]
 **serie** | [**string[]**](../Model/string.md)|  | [optional]
 **salesman** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DocumentListContainer**](../Model/DocumentListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **documenttypeList**
> \Swagger\Client\Model\DocumentTypeListContainer documenttypeList($type, $serie, $type1, $type2, $user_type, $user_id)

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DocumentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$type = "type_example"; // string | 
$serie = "serie_example"; // string | 
$type1 = "type1_example"; // string | 
$type2 = "type2_example"; // string | 
$user_type = "user_type_example"; // string | 
$user_id = "user_id_example"; // string | 

try {
    $result = $apiInstance->documenttypeList($type, $serie, $type1, $type2, $user_type, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentApi->documenttypeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **string**|  | [optional]
 **serie** | **string**|  | [optional]
 **type1** | **string**|  | [optional]
 **type2** | **string**|  | [optional]
 **user_type** | **string**|  | [optional]
 **user_id** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\DocumentTypeListContainer**](../Model/DocumentTypeListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

