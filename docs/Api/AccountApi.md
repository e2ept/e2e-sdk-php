# Swagger\Client\AccountApi

All URIs are relative to *https://e2e.pt/ws*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountCreate**](AccountApi.md#accountCreate) | **POST** /account | Create record
[**accountId**](AccountApi.md#accountId) | **GET** /account/id/{id} | Get record
[**accountList**](AccountApi.md#accountList) | **GET** /account | List records
[**currentAccountList**](AccountApi.md#currentAccountList) | **GET** /account/id/{id}/currentAccount | List current account records
[**currentAccountListType**](AccountApi.md#currentAccountListType) | **GET** /account/type/{type}/currentAccount | List current account records by account type


# **accountCreate**
> \Swagger\Client\Model\Account accountCreate($body)

Create record



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Account(); // \Swagger\Client\Model\Account | 

try {
    $result = $apiInstance->accountCreate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Account**](../Model/Account.md)|  |

### Return type

[**\Swagger\Client\Model\Account**](../Model/Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountId**
> \Swagger\Client\Model\Account accountId($id)

Get record



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->accountId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\Account**](../Model/Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountList**
> \Swagger\Client\Model\AccountListContainer accountList($offset, $limit, $name, $vat_number, $email, $type, $update_date, $deleted, $ids, $salesman)

List records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offset = 0; // int | 
$limit = 100; // int | 
$name = "name_example"; // string | 
$vat_number = "vat_number_example"; // string | 
$email = "email_example"; // string | 
$type = "type_example"; // string | 
$update_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601
$deleted = true; // bool | 
$ids = array("ids_example"); // string[] | 
$salesman = array("salesman_example"); // string[] | 

try {
    $result = $apiInstance->accountList($offset, $limit, $name, $vat_number, $email, $type, $update_date, $deleted, $ids, $salesman);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **name** | **string**|  | [optional]
 **vat_number** | **string**|  | [optional]
 **email** | **string**|  | [optional]
 **type** | **string**|  | [optional]
 **update_date** | **\DateTime**| ISO 8601 | [optional]
 **deleted** | **bool**|  | [optional]
 **ids** | [**string[]**](../Model/string.md)|  | [optional]
 **salesman** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AccountListContainer**](../Model/AccountListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **currentAccountList**
> \Swagger\Client\Model\CurrentAccountListContainer currentAccountList($id, $offset, $limit, $document_name, $document_type, $document_serie, $start_date, $end_date, $start_due_date, $end_due_date, $current_account_type, $account, $salesman)

List current account records



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 
$offset = 0; // int | 
$limit = 100; // int | 
$document_name = "document_name_example"; // string | 
$document_type = "document_type_example"; // string | 
$document_serie = "document_serie_example"; // string | 
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$start_due_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end_due_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$current_account_type = "current_account_type_example"; // string | 
$account = "account_example"; // string | 
$salesman = array("salesman_example"); // string[] | 

try {
    $result = $apiInstance->currentAccountList($id, $offset, $limit, $document_name, $document_type, $document_serie, $start_date, $end_date, $start_due_date, $end_due_date, $current_account_type, $account, $salesman);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->currentAccountList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **document_name** | **string**|  | [optional]
 **document_type** | **string**|  | [optional]
 **document_serie** | **string**|  | [optional]
 **start_date** | **\DateTime**|  | [optional]
 **end_date** | **\DateTime**|  | [optional]
 **start_due_date** | **\DateTime**|  | [optional]
 **end_due_date** | **\DateTime**|  | [optional]
 **current_account_type** | **string**|  | [optional]
 **account** | **string**|  | [optional]
 **salesman** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CurrentAccountListContainer**](../Model/CurrentAccountListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **currentAccountListType**
> \Swagger\Client\Model\CurrentAccountListContainer currentAccountListType($type, $offset, $limit, $document_name, $document_type, $document_serie, $start_date, $end_date, $start_due_date, $end_due_date, $current_account_type, $account, $salesman)

List current account records by account type



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$type = "type_example"; // string | 
$offset = 0; // int | 
$limit = 100; // int | 
$document_name = "document_name_example"; // string | 
$document_type = "document_type_example"; // string | 
$document_serie = "document_serie_example"; // string | 
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$start_due_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end_due_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$current_account_type = "current_account_type_example"; // string | 
$account = "account_example"; // string | 
$salesman = array("salesman_example"); // string[] | 

try {
    $result = $apiInstance->currentAccountListType($type, $offset, $limit, $document_name, $document_type, $document_serie, $start_date, $end_date, $start_due_date, $end_due_date, $current_account_type, $account, $salesman);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->currentAccountListType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **string**|  |
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **document_name** | **string**|  | [optional]
 **document_type** | **string**|  | [optional]
 **document_serie** | **string**|  | [optional]
 **start_date** | **\DateTime**|  | [optional]
 **end_date** | **\DateTime**|  | [optional]
 **start_due_date** | **\DateTime**|  | [optional]
 **end_due_date** | **\DateTime**|  | [optional]
 **current_account_type** | **string**|  | [optional]
 **account** | **string**|  | [optional]
 **salesman** | [**string[]**](../Model/string.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CurrentAccountListContainer**](../Model/CurrentAccountListContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

