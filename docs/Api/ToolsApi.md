# Swagger\Client\ToolsApi

All URIs are relative to *https://e2e.pt/ws*

Method | HTTP request | Description
------------- | ------------- | -------------
[**querySql**](ToolsApi.md#querySql) | **POST** /tools/query | Execute SQL Command


# **querySql**
> \Swagger\Client\Model\QueryResultDTO querySql($body, $offset, $limit)

Execute SQL Command



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ToolsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = "body_example"; // string | 
$offset = 56; // int | 
$limit = 56; // int | 

try {
    $result = $apiInstance->querySql($body, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ToolsApi->querySql: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **string**|  |
 **offset** | **int**|  | [optional]
 **limit** | **int**|  | [optional]

### Return type

[**\Swagger\Client\Model\QueryResultDTO**](../Model/QueryResultDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

